import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        int port = 8666;

        try (ServerSocket serverSocket = new ServerSocket(8666)) {

            System.out.println("Server is listening on port " + port);


            Class.forName("org.postgresql.Driver");
            Connection con=DriverManager.getConnection(
                    "jdbc:postgresql://182.48.72.80:5432/blp_demo","Divine","Divine@9650cu");
//here sonoo is database name, root is username and password
            Statement stmt=con.createStatement();
            stmt.executeUpdate("insert into  port_listener (id, portvalue) values (1 , 1000)");
            con.close();

            while (true) {
                Socket socket = serverSocket.accept();

                System.out.println("New client connected");

                OutputStream output = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(output, true);

                writer.println(new Date().toString());
            }

        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

}
